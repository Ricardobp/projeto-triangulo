package Triangulo;

public class App {
    public static void main(String args[]) {

        // A variável é do tipo Triangulo.Triangulo
        // Criar o triangulo chamando o construtor
        try {  // Quando quer testar possibilidades de erro
            Triangulo variavel1 = new Triangulo(5, 5, 15);
            Triangulo variavel2 = variavel1;
            Triangulo variavel3 = new Triangulo(5, 5, 15);
            Triangulo variavel4 = new Triangulo(1,2,3);     //Testando o erro botando c:0 e 1, 2 ,3
            System.out.println(variavel1);
            System.out.println(variavel2);
            System.out.println(variavel3);
            System.out.println(variavel4);

            if (variavel1 == variavel2) {
                System.out.println("== compara endereco de memoria -> São iguais (1)");
            }
            if (variavel1.equals(variavel3)) {
                System.out.println("equals compara objetos -> São iguais (2)");
            } else {
                System.out.println("equals compara objetos -> São diferentes (2)");
            }
            if (variavel1.equals("Glauco")) {
                System.out.println("equals compara objetos -> O professor é um triangulo.");
            } else {
                System.out.println("equals compara objetos -> O professor NÃO é um triangulo.");
            }

        } catch (ValoresInvalidos variavelDoErro) {
            System.out.println(variavelDoErro);
        }

    }
}