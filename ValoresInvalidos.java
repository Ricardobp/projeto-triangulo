package Triangulo;

public class ValoresInvalidos extends  Exception {  // criando o erro de valores Invalidos
    /**
     * Construtor que recebe um parametro
     * na heranca os construtores nao sao herdados
     */
    public ValoresInvalidos(String msg ) {
        super( msg );
    }
    public ValoresInvalidos(String msg, int a, int b, int c ) {
        super( msg + " a->" + a + " b->" + b + " c->" + c);
    }
}

