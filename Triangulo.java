package Triangulo;

public class Triangulo {
    private int a,b,c;
    /**
     * Construtor do Triangulo.Triangulo, não possui set para os valores
     * ele é imutável
     * @param a valor do lado a
     * @param b valor do lado b
     * @param c valor do lado c
     */
    public Triangulo(int a, int b, int c) throws ValoresInvalidos {
        if ((( b - c ) < a && a < (b + c )) &&
            (( a - c ) < b && b < (a + c )) &&
            (( a - b ) < c && c < (a + b ))){
            throw new ValoresInvalidos("Não é um triangulo", a , b ,c);
        }
        if( a == 0 || b == 0 || c == 0 ) {
            throw new ValoresInvalidos("Lado não pode ter valor zero");
        }

        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    @Override  //Não usa o equals do pai
    public boolean equals(Object obj) {
        Triangulo outro;
        boolean retorno = false;
        if (obj instanceof Triangulo) {
            outro = (Triangulo) obj;
            if(this.getA() == outro.getA() &&
                    this.getB() == outro.getB() &&
                    this.getC() == outro.getC()) {
                retorno = true;
            }
        }
        return retorno;
    }

    // @Override significa que estou substituindo algo do pai
    // neste caso o pai é Object
    @Override
    public String toString() {  //Para o codigo não imprimir o endereço de memoria da variavel
        return "Triangulo.Triangulo [ "  +
                " a: " + this.a +
                " b: " + this.b +
                " c: " + this.c + "]";
    }
}